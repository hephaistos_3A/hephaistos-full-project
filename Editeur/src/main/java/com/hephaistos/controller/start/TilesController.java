package com.hephaistos.controller.start;

import com.hephaistos.controller.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.util.ResourceBundle;

/**
 * Created by timotheearnauld on 11/02/2016.
 */
public class TilesController extends Controller implements Initializable {
    @FXML private GridPane tilesList;

    private FileChooser fileChooser;
    private EventHandler<MouseEvent> tileClic;

    public void init() {
        fileChooser = new FileChooser();

        tileClic = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ImageView temp = (ImageView) event.getSource();
                for(Node node : temp.getParent().getChildrenUnmodifiable()) {
                    if(node instanceof Label) {
                        Label temp2 = (Label) node;
                        System.out.println(temp2.getText());
                    }
                }
            }
        };

        initTilesList();


    }

    @FXML
    public void newTile(ActionEvent event) {
        File newTile = fileChooser.showOpenDialog(this.getPrimaryStage());
        if(newTile != null) {
            try {
                File temp = new File("src/main/resources/images/tiles/" + newTile.getName());
                Files.copy(newTile.toPath(), temp.toPath());
                addTileToList(newTile.getName(), "File:" + temp.getPath());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void initTilesList() {
        File f = new File("src/main/resources/images/tiles");
        File[] files = f.listFiles();

        for(int i=0;i<files.length;i++) {
            addTileToList(files[i].getName(),"File:" + files[i].getPath());
        }
    }

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        init();
    }

    public void addTileToList(String name,String path) {
        RowConstraints listRow = new RowConstraints();
        listRow.setMinHeight(100);
        listRow.setPrefHeight(100);
        listRow.setVgrow(Priority.NEVER);
        tilesList.getRowConstraints().add(listRow);

        GridPane newTile = new GridPane();
        newTile.setPrefSize(135,135);

        ColumnConstraints column = new ColumnConstraints();
        column.setPrefWidth(100);
        column.setMinWidth(10);
        column.setHalignment(HPos.CENTER);
        column.setHgrow(Priority.SOMETIMES);
        newTile.getColumnConstraints().add(0,column);

        RowConstraints row = new RowConstraints();
        row.setMaxHeight(162);
        row.setMinHeight(35);
        row.setPercentHeight(70);
        row.setPrefHeight(35);
        row.setValignment(VPos.BOTTOM);
        row.setVgrow(Priority.NEVER);
        newTile.getRowConstraints().add(0,row);

        row = new RowConstraints();
        row.setMaxHeight(160);
        row.setMinHeight(15);
        row.setPercentHeight(30);
        row.setPrefHeight(15);
        row.setValignment(VPos.TOP);
        row.setVgrow(Priority.NEVER);
        newTile.getRowConstraints().add(1,row);

        ImageView image = new ImageView(new Image(path));
        image.setFitWidth(60);
        image.setFitHeight(60);
        image.setPickOnBounds(true);
        image.setPreserveRatio(true);
        Label label = new Label(name);

        newTile.add(image,0,0);
        newTile.add(label,0,1);

        tilesList.add(newTile,0,tilesList.getChildren().size());

        image.setOnMouseClicked(tileClic);
    }
}