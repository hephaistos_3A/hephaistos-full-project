package com.hephaistos.controller.start;
//Modification par TAR pour test merge

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;
import javafx.stage.Screen;
import javafx.stage.StageStyle;

import javafx.geometry.Rectangle2D;
import java.io.IOException;

/**
 * Created by dim on 03/02/16.
 */
public class MainApp extends Application {

    private Stage rootStage;
    private GridPane mainLayout;

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage rootStage) throws Exception {

        this.rootStage = rootStage;
        rootStage.setTitle("Hephaistos");
	
        initRootLayout();
    }
   

    // Initialize the main Panel
    public void initRootLayout() throws IOException {


        /** Load Home start layout **/
        FXMLLoader loader = new FXMLLoader();
        // Set the fxml file to load
        loader.setLocation(MainApp.class.getResource("/view/start/login.fxml"));
        // Load layout
        mainLayout = loader.load();

        /** Set Controller
         *  we give access to the main Panel for the controller**/
        SignInController startController = loader.getController();
        startController.setStage(rootStage);
	    startController.setHostServices(getHostServices());


        /** Show the scene **/

        Scene scene = new Scene(mainLayout);

        rootStage.setScene(scene);
        rootStage.initStyle(StageStyle.UNDECORATED);
        rootStage.show();

	centerRootStage();

    }


    public void centerRootStage() {
	 Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        rootStage.setX((primScreenBounds.getWidth() - rootStage.getWidth()) / 2); 
        rootStage.setY((primScreenBounds.getHeight() - rootStage.getHeight()) / 2);  

	}

    // To display the user's project list
    public void getProjectLayout() throws IOException {
	    
	    FXMLLoader loader = new FXMLLoader();
	    loader.setLocation(MainApp.class.getResource("/view/start/projectsList.fxml"));

	    ScrollPane projectList = loader.load();
//	    mainLayout.setCenter(projectList);

    }


    public GridPane getMainLayout() {
        return mainLayout;
    }

    public Stage getRootStage() {
        return rootStage;
    }
}
