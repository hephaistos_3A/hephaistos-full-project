package com.hephaistos.controller.start;

import com.hephaistos.controller.Controller;
import javafx.stage.Stage;


/**
 * Created by timotheearnauld on 11/02/2016.
 */
public class ShapesController extends Controller{
    private Stage shapesStage;

    public void closeBase()
    {
        this.shapesStage.close();
    }
    // get the rootStage to change it
    public void setMainApp(Stage shapesStage) {
        this.shapesStage = shapesStage;
    }
}
