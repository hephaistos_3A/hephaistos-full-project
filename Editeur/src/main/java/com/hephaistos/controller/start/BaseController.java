package com.hephaistos.controller.start;

import com.hephaistos.controller.Controller;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

import static javafx.scene.control.SpinnerValueFactory.*;

/**
 * Created by timotheearnauld on 11/02/2016.
 */
public class BaseController extends Controller{
    private Stage baseStage;
    GridPane mainLayout;
    GraphicsContext context_map;
    GraphicsContext context_collisions;
    @FXML private Canvas canvas_map;
    @FXML private Canvas canvas_collisions;
    @FXML private GridPane loader;
    @FXML private Spinner brushSize;
    @FXML private Button paintButton;
    private int w;
    private int h;
    private boolean paint;
    private boolean delete;
    private boolean tiles;
    private boolean collisions;

    public BaseController()
    {
        paint = true;
        delete = false;
        tiles = true;
        collisions = false;
    }

    public void initBaseController(Stage baseStage, GridPane mainLayout)
    {
        this.baseStage = baseStage;
        canvas_map.setOnMouseDragged(mouseLeftEvent);
        canvas_collisions.setOnMouseDragged(mouseLeftEvent);
        this.mainLayout = mainLayout;
        context_map = canvas_map.getGraphicsContext2D();
        context_collisions = canvas_collisions.getGraphicsContext2D();
        SpinnerValueFactory sp = new IntegerSpinnerValueFactory(1, 200);
        brushSize.setValueFactory(sp);
        baseStage.getScene().widthProperty().addListener(new ChangeListener<Number>() {
        @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneWidth, Number newSceneWidth) {
            canvas_collisions.setWidth(newSceneWidth.doubleValue());
            canvas_map.setWidth(newSceneWidth.doubleValue());
            loader.setMinWidth(newSceneWidth.doubleValue());
        }
        });
        baseStage.getScene().heightProperty().addListener(new ChangeListener<Number>() {
            @Override public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                canvas_collisions.setHeight(newSceneHeight.doubleValue());
                canvas_map.setHeight(newSceneHeight.doubleValue());
                loader.setMinHeight(newSceneHeight.doubleValue());
            }
        });
    }

    @FXML private void hideCollisions()
    {
        if(collisions) {
            canvas_collisions.setVisible(false);
            collisions = false;
            return;
        }
        if(!collisions) {
            canvas_collisions.setVisible(true);
            collisions = true;
            return;
        }
    }

    @FXML private void saveMap()
    {
        canvas_collisions.setVisible(true);
        canvas_map.setVisible(true);
        loader.setVisible(true);
        //Take snapshot of the scene
        WritableImage wi_map = canvas_map.snapshot(null, null);

        // Write snapshot to file system as a .png image
        File uf_map = new File("canvas_map.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(wi_map, null), "png", uf_map);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        //Take snapshot of the scene
        WritableImage wi_collisions = canvas_collisions.snapshot(null, null);

        // Write snapshot to file system as a .png image
        File of_collisions = new File("canvas_collisions.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(wi_collisions, null), "png", of_collisions);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                loader.setVisible(false);
            }
        });
        new Thread(sleeper).start();
    }

    @FXML private void setPaint()
    {
        paint = true;
        delete = false;
    }

    @FXML private void setDelete()
    {
        delete = true;
        paint = false;
    }

    @FXML private void setCollisions()
    {
        canvas_collisions.setVisible(true);
        collisions = true;
        tiles = false;
    }

    @FXML private void setTiles()
    {
        tiles = true;
        collisions = false;
    }

    public void closeBase()
    {
        this.baseStage.close();
    }

    EventHandler<MouseEvent> mouseLeftEvent = new EventHandler<MouseEvent>()
    {
        @Override
        public void handle(MouseEvent mouseEvent) {
            w = Integer.parseInt(brushSize.getEditor().getText());
            h = w;
            int x = (int)(mouseEvent.getX() / w) * w;
            int y = (int)(mouseEvent.getY() / h) * h;
            if(paint && tiles)
            {
                paint(x, y);
            }
            if(paint && collisions)
            {
                paint_collisions(x, y);
            }
            if(delete && tiles)
            {
                delete(x, y);
            }
            if(delete && collisions)
            {
                delete_collisions(x, y);
            }
        }
    };

    private void paint(double x, double y)
    {
        context_map.setFill(Color.BLACK);
        context_map.fillRect(x,y,w, h);
    }

    private void paint_collisions(double x, double y)
    {
        context_collisions.setFill(Color.RED);
        context_collisions.fillRect(x,y,w, h);
    }

    private void delete(double x, double y)
    {
        context_map.clearRect(x,y, w, h);
    }

    private void delete_collisions(double x, double y)
    {
        context_collisions.clearRect(x,y, w, h);
    }

    // get the rootStage to change it
    public void setMainApp(Stage baseStage) {
        this.baseStage = baseStage;
    }
}
