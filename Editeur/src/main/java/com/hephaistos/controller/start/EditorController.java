package com.hephaistos.controller.start;

import com.hephaistos.controller.Controller;
//import com.sun.xml.internal.rngom.parse.host.Base;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * Created by timotheearnauld on 11/02/2016.
 */
public class EditorController extends Controller{
    private Stage editorStage;
    @FXML private CheckMenuItem baseButton;
    @FXML private CheckMenuItem shapesButton;
    @FXML private CheckMenuItem tilesButton;
    private Stage baseStage;
    private ShapesController shapesController;
    private TilesController tilesController;
    private Stage tilesStage;

    public EditorController()
    {
    }

    @FXML
    public void setBaseOn() throws IOException
    {
        if(baseButton.isSelected())
        {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(BaseController.class.getResource("/view/main/base.fxml"));
            GridPane mainLayout = loader.load();
            Scene scene = new Scene(mainLayout);

            BaseController baseController = loader.getController();
            baseController.setMainApp(baseStage);

            baseStage = new Stage();
            baseStage.setScene(scene);
            baseStage.initStyle(StageStyle.DECORATED);
            baseStage.show();
            baseStage.setTitle("Base");
            baseController.initBaseController(baseStage, mainLayout);
            //todo Fenetre au premier plan.

            //BaseController baseController = new BaseController();
            //openPane("/view/main/base.fxml", baseController, "Base");

            //Waiting System
        }

        else
        {
            baseStage.close();
        }
    }

    @FXML
    public void setShapesOn() throws IOException
    {
        if(shapesButton.isSelected())
        {
            /*FXMLLoader loader = new FXMLLoader();
            loader.setLocation(ShapesController.class.getResource("/view/start/shapes.fxml"));
            GridPane mainLayout = loader.load();

            Scene scene = new Scene(mainLayout);

            shapesStage = new Stage();
            shapesStage.setScene(scene);
            shapesStage.initStyle(StageStyle.DECORATED);
            shapesStage.show();

            ShapesController shapesController = loader.getController();
            shapesController.setMainApp(shapesStage);*/
            //Waiting System

            shapesController = new ShapesController();
            openPane("/view/main/shapes.fxml", shapesController, "Shapes");

            //centerRootStage();
        }
        else
        {
            //shapesStage.close();
        }
    }

    @FXML
    public void setTilesOn() throws IOException
    {
        if(tilesButton.isSelected())
        {
            /*FXMLLoader loader = new FXMLLoader();
            loader.setLocation(TilesController.class.getResource("/view/start/tiles.fxml"));
            GridPane mainLayout = loader.load();

            Scene scene = new Scene(mainLayout);

            tilesStage = new Stage();
            tilesStage.setScene(scene);
            tilesStage.initStyle(StageStyle.DECORATED);
            tilesStage.show();

            TilesController tilesController = loader.getController();
            tilesController.setMainApp(tilesStage);*/
            //Waiting System

            tilesController = new TilesController();
            openPane("/view/main/tiles.fxml", tilesController, "Tiles");


        }
        else
        {

            tilesStage.close();
        }
    }

    @FXML
    public void disconnectUser()
    {
        try
        {
            returnToMainPane();
        }
        catch (IOException e)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("An error occured. Application is going to close.");
            editorStage.close();
        }
    }

    @FXML
    public void closeApp()
    {
        Platform.exit();
        System.exit(0);
    }

    // Go to the editor panel
    public void returnToMainPane() throws IOException {
       /* FXMLLoader loader = new FXMLLoader();
        loader.setLocation(SignInController.class.getResource("/view/start/login.fxml"));
        GridPane mainLayout = loader.load();

        Scene scene = new Scene(mainLayout);*/
        editorStage.close();

        /*Stage rootStage = new Stage();
        rootStage.setScene(scene);
        rootStage.initStyle(StageStyle.UNDECORATED);
        rootStage.show();

        //Set Editor Controller
        SignInController signInController= loader.getController();
        signInController.setStage(rootStage);*/
        //Waiting System

        SignInController signInController = new SignInController();
        openPane("/view/start/login.fxml", signInController);

        //centerRootStage();

    }

    @Deprecated
    public void centerRootStage() {
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        editorStage.setX((primScreenBounds.getWidth() - editorStage.getWidth()) / 2);
        editorStage.setY((primScreenBounds.getHeight() - editorStage.getHeight()) / 2);

    }

    // get the rootStage to change it
    public void setMainApp(Stage editorStage) {
        this.editorStage = editorStage;
    }
}
