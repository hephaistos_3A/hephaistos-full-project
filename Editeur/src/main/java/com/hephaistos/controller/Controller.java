package com.hephaistos.controller;

import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

import javafx.stage.Screen;
import javafx.stage.StageStyle;
import javafx.stage.Stage;

import javafx.application.Platform;
import java.io.IOException;




public abstract class Controller {

	private Stage stage;
	private Stage newStage;


	public void setStage(Stage stage) {
		this.stage = stage;
	}

	@FXML
	public void closeApp() {
		
		/** Vérification fermeture application */
		Platform.exit();
		System.exit(0);
	}

	public void closeStage() {
			newStage.close();
	}

	public void openPane(String resourceView, Controller controller) throws IOException {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Controller.class.getResource(resourceView));
		GridPane layout = loader.load();

		Scene scene = new Scene(layout);

		stage.setScene(scene);
		//stage.initStyle(StageStyle.DECORATED);
		stage.show();

		controller = loader.getController();
		controller.setStage(stage);

		centerRootStage();

	}

	public void openPane(String resourceView, Controller controller, String titlePane) throws IOException {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Controller.class.getResource(resourceView));
		GridPane layout = loader.load();

		Scene scene = new Scene(layout);

		newStage = new Stage();
		newStage.setScene(scene);
		newStage.initStyle(StageStyle.DECORATED);
		newStage.setTitle(titlePane);
		newStage.show();

		controller = loader.getController();
		controller.setStage(newStage);
	}


	public void centerRootStage() {
		Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
		stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
		stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);

	}

	public Stage getPrimaryStage() {
		return stage;
	}

	public Controller getController() {
		return this;
	}


}
