package com.hephaistos;

import com.hephaistos.controller.Controller;
import javafx.stage.Stage;

/**
 * Created by dim-home on 2/20/16.
 */
public interface Controls {

    public void setStage(Stage stage);
    public void closeApp();
    public void openPane(String resourceView, Controller controller);

}
