package test;

import directory.Directory;
import email.Email;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by timotheearnauld on 26/04/2016.
 */

public class TestComplete {
    /*
    * Test if the directory is complete
    * */

    public void launchTest(){
        ArrayList<String> waiting = new ArrayList<String>();
        TestComplete test = new TestComplete();

        Directory dir = new Directory();
        waiting = dir.listFiles("tmp/");

        for(String e : waiting){
            System.out.print(e + ": ");
            if(test.isDirectoryValid(e))
                System.out.println("is valid.");
            else System.out.println("is not valid.");
        }

        System.out.println("There are " + waiting.size() + " scenarios...");
    }

    public boolean isDirectoryValid(String path){

        if(isRootValid(path)){
            return true;
        }

        return false;
    }

    public boolean isRootValid(String path){
        ArrayList<String> names = new ArrayList<String>();
        Directory dir = new Directory();
        int valid = 0;

        names = dir.listFiles(path);

        for(String e : names){
            try {
                if (e.substring(e.lastIndexOf("."), e.length()).compareTo(".hpts") == 0) {
                    if (isHephaistosValid(e))
                        valid++;
                }

                if (e.substring(e.lastIndexOf("."), e.length()).compareTo(".json") == 0) {
                    if (isJSONValid(e))
                        valid++;
                }
            }
            catch(Exception ex){

            }

            if(e.contains("/cfg")) {
                if(isConfigValid(e))
                    valid++;
                else System.out.println(path + " config folder is not valid.");
            }

            if(e.contains("/rsc")) {
                if(isRessourcesValid(e))
                    valid++;
                else System.out.println(path + " ressources folder is not valid.");
            }
        }

        Email email = new Email();

        if(valid == 4) {
            email.sendSuccess();
            return true;
        }

        else {
            email.sendError();
            return false;
        }
    }

    public boolean isConfigValid(String path){
        Directory dir = new Directory();
        ArrayList<String> files = new ArrayList<String>();
        files= dir.listFiles(path);

        if(files.size() == 0)
            return false;

        for(String e : files){
            if(e.substring(e.lastIndexOf("."), e.length()).compareTo(".cfg") != 0)
                return false;

            File f = new File(e);
            if (f.isDirectory()) {
                System.out.println(path + " : countains directory");
                return false;
            }
        }

        return true;
    }

    public boolean isRessourcesValid(String path){
        Directory dir = new Directory();
        ArrayList<String> files = new ArrayList<String>();
        files= dir.listFiles(path);

        int valid = 0;

        if(files.size() == 0)
            return false;

        for(String e : files){
            if(e.contains("img")){
                if(dir.onlyContainsImages(e))
                    valid++;
                else System.out.println(path + " does not only contains images");
            }

            if(e.contains("musics")){
                if(dir.onlyContainsMusics(e))
                    valid++;
                else System.out.println(path + " :does not only contains musics");
            }
        }

        if(valid == 2)
            return true;

        return false;
    }

    public boolean isHephaistosValid(String path){
        Validation test = new Validation();
        if(!test.checkHptsFile(path)){
            System.out.println(path + " xml is not valid");
            return false;
        }


        return true;
    }

    public boolean isJSONValid(String path){
        Validation test = new Validation();
        if(!test.checkJSONFile(path))
        {
            System.out.println(path + " json is not valid");
            return false;
        }

        return true;
    }
}
