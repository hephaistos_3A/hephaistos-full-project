package test;

import files.Files;

/**
 * Created by timotheearnauld on 26/04/2016.
 */
public class Validation {
    /*A public class which allow to test the xml files validation.
    and with a ressources's consideration.
    */

    public boolean checkHptsFile(String path){
        Files f = new Files();
        if(!f.isXMLValid(path)){
            return false;
        }
        return true;
    }

    public boolean checkJSONFile(String path){
        Files f = new Files();
        if (!f.isJSONValid(path)) {
            return false;
        }
        return true;
    }
}
