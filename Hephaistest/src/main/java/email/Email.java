package email;

import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class Email
{
    public void sendEmail(String t, String subject, String content)
    {
        String to = t;
        String from = "web@gmail.com";
        String host = "localhost";

        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", host);

        Session session = Session.getDefaultInstance(properties);

        try{
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.setSubject(subject);
            message.setContent(content, "text/html" );

            Transport.send(message);
            System.out.println("Sent message successfully....");
        }
        catch (Exception e){
            System.out.println("Email cannot be send.");
        }
    }

    public void sendSuccess(){
        try{
            sendEmail("", "", "");
        }
        catch(Exception e){
            System.out.println("Message can't be send...");
        }
    }

    public void sendError(){
        try{
            sendEmail("", "", "");
        }
        catch(Exception e){
            System.out.println("Message can't be send...");
        }
    }
}