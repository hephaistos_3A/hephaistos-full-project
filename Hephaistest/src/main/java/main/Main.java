package main;

import test.TestComplete;

/**
 * Created by timotheearnauld on 26/04/2016.
 */

public class Main {
    public static void main(String[]args){
        TestComplete t = new TestComplete();
        t.launchTest();
    }
}
