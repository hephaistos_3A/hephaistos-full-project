package files;

import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Created by timotheearnauld on 26/04/2016.
 */

public class Files {

    public boolean isXMLValid(String path) {
        File temp = new File(path);
        try{
            String passed_xml_string = readFile(path);

            if(passed_xml_string.isEmpty())
                return false;

            BufferedWriter bw = null;
            Document doc = null;
            try
            {
                PrintWriter w = new PrintWriter(temp);
                bw = new BufferedWriter(w);
                bw.write(passed_xml_string);
                bw.flush();

                doc = null;
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                doc = builder.parse(temp);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        catch(IOException e){
            return false;
        }
    }

    public boolean isJSONValid(String path){
        try{
            String passed_json_string = readFile(path);

            if(passed_json_string.isEmpty())
                return false;

            JsonParser parser = new JsonParser();
            parser.parse(passed_json_string);
            return true;
        }
        catch(IOException e){
            System.out.println("File not found...");
            return false;
        }
        catch(JsonSyntaxException jse){
            System.out.println("Not a valid Json String:"+jse.getMessage());
            return false;
        }
    }

    private String readFile(String pathname) throws IOException {

        File file = new File(pathname);
        StringBuilder fileContents = new StringBuilder((int)file.length());
        Scanner scanner = new Scanner(file);
        String lineSeparator = System.getProperty("line.separator");

        try {
            while(scanner.hasNextLine()) {
                fileContents.append(scanner.nextLine() + lineSeparator);
            }
            return fileContents.toString();
        } finally {
            scanner.close();
        }
    }

}
