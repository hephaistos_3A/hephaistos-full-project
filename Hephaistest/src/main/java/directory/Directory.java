package directory;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by timotheearnauld on 26/04/2016.
 */
public class Directory {

    public ArrayList listFiles(String folder) {
        //Allow to list a directory
        //Return a string array list with scenarios' path;

        ArrayList<String> names = new ArrayList<String>();

        File directory = new File(folder);
        File[] contents = directory.listFiles();

        for (File f : contents) {
            if(!f.toString().contains("/.")) {
                names.add(f.toString());
            }
        }
        return names;
    }

    public boolean onlyContainsMusics(String folder){
        Directory dir = new Directory();
        ArrayList<String> files = new ArrayList<String>();
        files= dir.listFiles(folder);

        for(String e : files){
            if(e.substring(e.lastIndexOf("."), e.length()).compareTo(".mp3") != 0)
                return false;

            File f = new File(e);
            if (f.isDirectory()) {
                return false;
            }
        }

        return true;
    }

    public boolean onlyContainsImages(String folder){
        Directory dir = new Directory();
        ArrayList<String> files = new ArrayList<String>();
        files= dir.listFiles(folder);

        if(files.size() == 0)
            return false;

        for(String e : files){
            if(e.substring(e.lastIndexOf("."), e.length()).compareTo(".jpg") != 0 && e.substring(e.lastIndexOf("."), e.length()).compareTo(".jpeg") != 0 && e.substring(e.lastIndexOf("."), e.length()).compareTo(".png") != 0 )
                return false;

            File f = new File(e);
            if (f.isDirectory()) {
                return false;
            }
        }

        return true;
    }
}
