package com.hephaistos.model.properties;

import javafx.stage.StageStyle;

/**
 * Created by Dimitri on 23/02/2016.
 */
public class StageProperties {

    protected double width;
    protected double height;
    protected double layoutX;
    protected double layoutY;
    protected StageStyle style;
    protected String title;


    public void setWidth(double width) {
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setLayoutX(double layoutX) {
        this.layoutX = layoutX;
    }

    public double getLayoutX() {
        return layoutX;
    }

    public void setLayoutY(double layoutY) {
        this.layoutY = layoutY;
    }

    public double getLayoutY() {
        return layoutY;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setStyle(StageStyle style){
        this.style = style;
    }

    public StageStyle getStyle() {
        return style;
    }
}
