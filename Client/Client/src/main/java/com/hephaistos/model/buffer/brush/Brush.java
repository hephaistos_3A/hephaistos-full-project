package com.hephaistos.model.buffer.brush;

import com.hephaistos.model.buffer.EditorBuffer;

import java.awt.*;

/**
 * Created by Guillaume on 21/02/2016.
 */
public class Brush implements EditorBuffer {
    private String name;
    private Image tile;

    public void setName(String _name) {name = _name;}
    public void setTile(Image _tile) {tile = _tile;}

    public String getName() {return name;}
    public Image getTile() {return tile;}
}
