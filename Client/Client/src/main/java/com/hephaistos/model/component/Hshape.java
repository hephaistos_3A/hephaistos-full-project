package com.hephaistos.model.component;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

/**
 * Created by dim-home on 2/21/16.
 */
public class Hshape {

    private Shape shape;
    private Color color;


    public void setShape(Shape shape) {
        this.shape = shape;

        shape.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                /** GET SHAPE PROPERTIES **/
                System.out.println("GET PROPERTIES");

            }
        });
    }

    public Shape getShape() {
        return shape;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
