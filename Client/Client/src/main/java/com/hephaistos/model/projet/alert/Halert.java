package com.hephaistos.model.projet.alert;

import com.hephaistos.controller.Controller;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.util.Optional;

/**
 * Created by dim-home on 2/21/16.
 */
public class Halert {

    private Alert alert;

    public Halert(Controller controller, String type){

        switch(type) {
            case "confirmation":
                alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("CONFIRMATION");
                alert.setContentText("Do you want to save this project ?");

                ButtonType buttonTypeOne = new ButtonType("Don't save");
                ButtonType buttonTypeTwo = new ButtonType("Save");
                ButtonType buttonTypeThree = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);


                alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree);
                Optional<ButtonType> result = alert.showAndWait();

                if(result.get() == buttonTypeOne) {
                    Platform.exit();
                    System.exit(0);
                }

                else if(result.get() == buttonTypeTwo) {

                    Platform.exit();
                    System.exit(0);
                }
                break;

        }

    }

}
