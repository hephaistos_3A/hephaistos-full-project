package com.hephaistos.model.bufferContainer;

import com.hephaistos.model.buffer.EditorBuffer;

import java.util.HashMap;

/**
 * Created by Guillaume on 21/02/2016.
 */
public class BufferContainer {
    private HashMap<String,EditorBuffer> buffers;

    public BufferContainer() {
        buffers = new HashMap<>();
    }

    public void addBuffer(String name,EditorBuffer buffer) {
        buffers.put(name,buffer);
    }

    public EditorBuffer getBuffer(String name) {
        return buffers.get(name);
    }
}
