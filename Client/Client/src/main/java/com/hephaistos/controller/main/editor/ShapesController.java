package com.hephaistos.controller.main.editor;

import com.dtg.controller.DTGController;

import com.dtg.controller.DTGController;

import com.hephaistos.model.component.Hshape;
import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import javafx.stage.Stage;


/**
 * Created by timotheearnauld on 11/02/2016.
 */

public class ShapesController extends DTGController {

    private Stage shapesStage;

    @FXML private GridPane shapesList;

    public  ShapesController() {}

    public ShapesController(Stage stage) {
        super(stage);
    }

    @FXML
    public void addShape() {

        Hshape circle = new Hshape();
        circle.setShape(new Circle(20,20,20));
        circle.setColor(Color.BLUE);

        Hshape rect = new Hshape();
        rect.setShape(new Rectangle(50,50));
        rect.setColor(Color.BLUE);



        shapesList.add(circle.getShape(), 0, 0);
        shapesList.add(rect.getShape(), 1, 0);
    }

}
