package com.hephaistos.controller.start;

import javafx.animation.ScaleTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ScrollPane;
import javafx.animation.FadeTransition;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.stage.StageStyle;

import javafx.geometry.Rectangle2D;

import javafx.stage.Stage;
import javafx.stage.Screen;
import javafx.application.HostServices;
import javafx.application.Platform;

import java.sql.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.IOException;

import com.hephaistos.model.user.User;
import com.hephaistos.controller.Controller;

/**
 * Created by dim on 03/02/16.
 * Commit
 */
public class SignInController extends Controller{

    private Stage rootStage;
    private HostServices hostServices;

    @FXML private TextField loginField;
    @FXML private PasswordField passwordField;
    @FXML private Button connectButton;
    @FXML private Label signError; 
    @FXML private Label signInOk;
    
   	  private String login;
	  private String pass;
	  private User user;

    
    public SignInController() {
	   
    }
	
    // get the rootStage to change it
    //public void setMainApp(Stage rootStage) {
       /* this.rootStage = rootStage;
    }*/
	
    public void setHostServices(HostServices hostServices) {
	    this.hostServices = hostServices;
    }

    // Control all field is not empty	    
    @FXML
    public void controlSignIn(){
	login = loginField.getText();
   	pass = passwordField.getText();

	signError.setVisible(false);

	if(!login.equals("") && !pass.equals(""))
			connectButton.setDisable(false);
	else
			connectButton.setDisable(true);
    }


    //For now access to the database todo : seperate database connection and query
    @FXML
    public void getConnection() throws ConnectException {
	String url = "jdbc:mysql://localhost:8889/hepaistos";
	String sqlLogin = "root";
	String sqlPassword = "root";

	Connection cn = null;
	Statement st = null;
	ResultSet rs = null;
	
	String login = loginField.getText();
	String pass = passwordField.getText();
	try {

		// Load JDBC Driver
		Class.forName("com.mysql.jdbc.Driver");
		//get connection
		cn = DriverManager.getConnection(url, sqlLogin, sqlPassword);
		// Create statement
		st = cn.createStatement();
		String sql = "SELECT * FROM user WHERE login = '" + login + "'";
		// Execute query
		rs = st.executeQuery(sql);
		
		ResultSetMetaData resultMeta = rs.getMetaData();
		
		// todo if no login ConnectionException | refractor the exception catch
		// for now if login match
		if(rs.next() && resultMeta.getColumnCount() > 0){
			
			
		MessageDigest md = MessageDigest.getInstance("SHA1");

		md.update(pass.getBytes());
		byte[] byteData = md.digest();
		StringBuilder sb = new StringBuilder();
		
		// append for SHA1 convertion of password
		for(int i = 0; i < byteData.length; i++) {
			sb.append(String.format("%02x", byteData[i] & 0xff));
		}

		pass = sb.toString();
		
		// if password match
		if(pass.equals(rs.getObject("password").toString())) {
			
			showSignIn();
			String id = rs.getObject("id").toString();
			String pseudo = rs.getObject("pseudo").toString();
			String avatar = rs.getObject("avatar").toString();

			user = setUser(id, pseudo, avatar);

            showMainPane();
		
			
		}else{
			throw new ConnectException();
		}
            
		}else{
			throw new ConnectException();
		}



	
	}catch (ConnectException e) {
		e.showError(signError);
	}catch (SQLException e) {
		e.printStackTrace();
	}catch (NoSuchAlgorithmException e) {
		e.printStackTrace();	
	}catch (ClassNotFoundException e) {
		e.printStackTrace();
	}catch (IOException e) {
		e.printStackTrace();
	}finally {
		//free memory resource
		try {
			cn.close();
			st.close();
		}catch (SQLException e){
			e.printStackTrace();
		}
	}

	    }
    
    // Set instance of User
    public User setUser(String id, String pseudo, String avatar) {
	
    	user = new User();	    
	user.setId(id);
	user.setPseudo(pseudo);
	user.setAvatar(avatar);

	return user;	
    }

    public void showSignIn(){
	signInOk.setVisible(true);
    }

    /**
     * @FMLX access from fxml file
     */
    // Return to the Start panel
    @FXML
    public void showStartPane() throws IOException {


        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(SignInController.class.getResource("/view/start/.fxml"));
        BorderPane startLayout = loader.load();

        StartController startController = loader.getController();
        startController.setStage(rootStage);
		startController.setConnectUser(user);
		startController.setHostServices(hostServices);

        Scene scene = new Scene(startLayout);
        rootStage.setScene(scene);
        rootStage.show();

	
    }
	
    // To display the user's project list
    public void getProjectLayout() throws IOException {

	// A faire : récupérer MainApp class --> fonction

    }

    // Go to the editor panel    
    public void showMainPane() throws IOException {
	
	
/*	FXMLLoader loader = new FXMLLoader();
	loader.setLocation(EditorController.class.getResource("/view/main/editor.fxml"));
	GridPane mainLayout = loader.load();*/

	/*FadeTransition ft = new FadeTransition(Duration.millis(2000),mainLayout);
	ft.setFromValue(0.0);
	ft.setToValue(1.0);
	ft.play();*/
	
/*	Scene scene = new Scene(mainLayout);
	rootStage.close();

	Stage editorStage = new Stage();
	editorStage.setScene(scene);
	editorStage.initStyle(StageStyle.DECORATED);
	editorStage.show();*/

	//Set Editor Controller
	/*EditorController editorController = loader.getController();
	editorController.setMainApp(editorStage);*/
	//Waiting System
	EditorController editorController = new EditorController();
	openPane("/view/main/editor.fxml", editorController);


	
	}
	


    public void sleepThread(int value) {
	
	    try {
		Thread.sleep(value);
	    }catch(InterruptedException e) {
		System.out.println("Error Load");
	    }
    }


    // open the create account page with the default browser
   @FXML
   public void createAccount() {
	   
	hostServices.showDocument("http://localhost/h-pha-stos-website/H/app");

   }

    @FXML
    public void closeApp() {
	Platform.exit();
	System.exit(0);
    }

    public User getUser() {
	    return user;
    }
}

@SuppressWarnings("serial")
class ConnectException extends RuntimeException {

	public ConnectException() {
	
	};
	
	// display the login/password Label error
	public void showError(Label fieldError) {
			
		fieldError.setVisible(true);
	}

}
