package com.hephaistos.controller.sign;


import com.dtg.controller.DTGController;
import com.dtg.graphic.utility.WindowUtility;
import com.hephaistos.controller.main.editor.EditorController;

import javafx.application.Platform;
import javafx.fxml.FXML;

import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Label;
import javafx.scene.control.Button;

import javafx.application.HostServices;

import java.sql.*;
import java.io.IOException;

import com.hephaistos.model.user.User;

import com.hephaistos.controller.Controller;
import javafx.scene.layout.Region;

import javafx.stage.Stage;

import javafx.stage.StageStyle;


public class SignInController extends DTGController{


    private HostServices hostServices;

    @FXML private TextField loginField;
    @FXML private PasswordField passwordField;
    @FXML private Button connectButton;
    @FXML private Label signError; 
    @FXML private Label signInOk;
    
   	  private String login;
	  private String pass;
	  private User user;


	public SignInController() {}

	public SignInController(Stage stage){
		super(stage);
	}


    public void setHostServices(HostServices hostServices) {
	    this.hostServices = hostServices;
    }


	/**
	 * Open Editor window
 	 * @throws IOException
     */
    public void setEditor() throws IOException {


		EditorController editorController = new EditorController(new Stage());
		WindowUtility.openStage("/view/main/editor.fxml", editorController, StageStyle.UNDECORATED, false);

		this.close(1);
	}


	/**
	 * Control if all field is not empty
	 */
	@FXML
	public void controlSignIn(){
		login = loginField.getText();
		pass = passwordField.getText();

		signError.setVisible(false);

		if(!login.equals("") && !pass.equals(""))
			connectButton.setDisable(false);
		else
			connectButton.setDisable(true);
	}


	//For now access to the database todo : seperate database connection and query

	/**
	 *
	 * @throws ConnectException
	 * @throws IOException
     */
	@FXML
	public void getConnection() throws ConnectException, IOException{
		String url = "jdbc:mysql://192.168.1.77:3306/hephaistos";
		String sqlLogin = "root";
		String sqlPassword = "root";

		Connection cn = null;
		Statement st = null;
		ResultSet rs = null;

		String login = loginField.getText();
		String pass = passwordField.getText();
		/*try {

			// Load JDBC Driver
			Class.forName("com.mysql.jdbc.Driver");
			//get connection
			cn = DriverManager.getConnection(url, sqlLogin, sqlPassword);
			// Create statement
			st = cn.createStatement();
			String sql = "SELECT * FROM user WHERE login = '" + login + "'";
			// Execute query
			rs = st.executeQuery(sql);

			ResultSetMetaData resultMeta = rs.getMetaData();

			// todo if no login ConnectionException | refractor the exception catch
			// for now if login match
			if(rs.next() && resultMeta.getColumnCount() > 0){


				MessageDigest md = MessageDigest.getInstance("SHA1");

				md.update(pass.getBytes());
				byte[] byteData = md.digest();
				StringBuilder sb = new StringBuilder();

				// append for SHA1 convertion of password
				for(int i = 0; i < byteData.length; i++) {
					sb.append(String.format("%02x", byteData[i] & 0xff));
				}

				pass = sb.toString();

				// if password match
				if(pass.equals(rs.getObject("password").toString())) {

					showSignIn();
					String id = rs.getObject("id").toString();
					String pseudo = rs.getObject("pseudo").toString();
					String avatar = rs.getObject("avatar").toString();

					user = setUser(id, pseudo, avatar);

					setEditor();

				}else{
					throw new ConnectException();
				}

			}else{
				throw new ConnectException();
			}

		}catch (ConnectException e) {
			e.showError(signError);
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (IOException e) {
			e.printStackTrace();
		}finally {
			//free memory resource
			try {
				cn.close();
				st.close();
			}catch (SQLException e){
				e.printStackTrace();
			}
		}*/

		setEditor();
	}


	/**
	 * Set instance of user
	 * @param id
	 * @param pseudo
	 * @param avatar
     * @return
     */

	public User setUser(String id, String pseudo, String avatar) {

		user = new User();
		user.setId(id);
		user.setPseudo(pseudo);
		user.setAvatar(avatar);

		return user;
	}

	public void showSignIn(){
		signInOk.setVisible(true);
	}

	// open the create account page with the default browser
	@FXML
	public void createAccount() {

		hostServices.showDocument("http://localhost/h-pha-stos-website/H/app");

	}

	@Deprecated
	public void sleepThread(int value) {
	
	    try {
		Thread.sleep(value);
	    }catch(InterruptedException e) {
		System.out.println("Error Load");
	    }
    }

	@FXML
	public void closeApp() {
		Platform.exit();
		System.exit(0);
	}

    public User getUser() {
	    return user;
    }
	
}

@SuppressWarnings("serial")
class ConnectException extends RuntimeException {

	public ConnectException() {}
	
	// display the login/password Label error
	public void showError(Label fieldError) {
			
		fieldError.setVisible(true);
	}

}
