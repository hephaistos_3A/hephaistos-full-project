package com.hephaistos.controller.start;

import com.hephaistos.controller.Controller;
import javafx.stage.Stage;

/**
 * Created by timotheearnauld on 11/02/2016.
 */
public class TilesController extends Controller{
    private Stage tilesStage;

    public void closeBase()
    {
        this.tilesStage.close();
    }
    // get the rootStage to change it
    public void setMainApp(Stage tilesStage) {
        this.tilesStage = tilesStage;
    }
}
