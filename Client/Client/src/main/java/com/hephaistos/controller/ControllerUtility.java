package com.hephaistos.controller;


import com.dtg.graphic.window.DTGWindow;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by DTG on 25/02/2016.
 */
public class ControllerUtility {



    public void closeStage(Controller controller) {

        controller.closeStage();
    }

    public FXMLLoader initView(String resourceView) {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Controller.class.getResource(resourceView));

        return loader;
    }

    public Object openStage(String resourceView, String  controller, StageStyle style, boolean custom, boolean inside) throws IOException {

        Class c;
        Constructor constructor;
        Object obj;
        Method setStage, getStage, getController, initContent;


        try{

            /** Get contoller class if exist */
            c = Class.forName(controller);
            constructor = c.getConstructor();

            /** create new ControllerClass **/
            obj = constructor.newInstance();

            /** Load the FXML view **/
            FXMLLoader loader = initView(resourceView);

            /** apply the controller to the view **/
            loader.setController(obj);

            /** get the view to a code region **/
            Region root = loader.load();

            /** Save the controller's stage **/
            setStage = c.getMethod("setStage", Stage.class);
            Stage stage = new Stage();
            setStage.invoke(obj,stage);


            /** Get the controller **/
            getController = c.getMethod("getController");
            Controller control = (Controller)getController.invoke(obj);

            if(custom) {
                /** Use graphical dtgframework for Hephaistos window **/

                initContent = c.getMethod("initContent");
                initContent.invoke(obj);

                getStage = c.getMethod("getStage");
                DTGWindow dtgWindow = new DTGWindow(stage, root, inside);

                dtgWindow.getStylesheets().add(ControllerUtility.class.getResource("/view/dtgWindow/skin/undecorator.css").toString());

                /**
                 * TODO initialiser dans la classe les informations de dimension
                 */
                Scene scene = new Scene(dtgWindow, getHostWidth(), getHostHeight());
                scene.setFill(Color.TRANSPARENT);
                stage.initStyle(StageStyle.TRANSPARENT);
                stage.setScene(scene);


            }else{

                Scene scene = new Scene(root);
                stage.initStyle(style);
                stage.setScene(scene);

            }

            /** Show **/
            stage.show();

            return obj;


        }catch(InstantiationException e) {
            System.out.println("Can not create new instance of " + controller);
        }catch(ClassNotFoundException e){
            System.out.println(controller + " not found");
        }catch(NoSuchMethodException e) {
            e.printStackTrace();
            System.out.println("No method found");
        }catch(IllegalAccessException e){
            System.out.println("Can not access to this method");
        }catch(InvocationTargetException e){
            e.printStackTrace();
            System.out.println("Can not invoke this method");
        }

        return null;

    }

    public void openInsideWindow(String resourceView, String controller, String title) throws  IOException{

       openStage(resourceView, controller, StageStyle.UNDECORATED,true, true);

    }

    public double getHostWidth() {
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        return primScreenBounds.getWidth();
    }

    public double getHostHeight() {
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        return primScreenBounds.getHeight();
    }
}
