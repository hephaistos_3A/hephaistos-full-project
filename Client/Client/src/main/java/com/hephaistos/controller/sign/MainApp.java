package com.hephaistos.controller.sign;
//Modification par TAR pour test merge

import com.hephaistos.controller.ControllerUtility;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.Screen;

import javafx.geometry.Rectangle2D;
import javafx.stage.StageStyle;

import java.io.IOException;



/**
 * Created by dim on 03/02/16.
 */
public class MainApp extends Application {

   
    public static void main(String[] args) {
        launch(args);
    }

 
    /**
     * Launch application, start with login window
     * @param rootStage
     * @throws Exception
     */
    @Override
    public void start(Stage rootStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource("/view/start/login.fxml"));

        SignInController signInController = new SignInController(rootStage);
        loader.setController(signInController);

        Region root = loader.load();


        Scene scene = new Scene(root, 400, 400);
        signInController.getStage().setScene(scene);
        signInController.getStage().show();

    }
    

 }
