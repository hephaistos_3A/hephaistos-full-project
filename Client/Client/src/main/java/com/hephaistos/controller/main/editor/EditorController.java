package com.hephaistos.controller.main.editor;

import com.dtg.controller.DTGController;
import com.dtg.graphic.utility.WindowUtility;

import com.hephaistos.controller.Controller;
import com.hephaistos.controller.sign.SignInController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * Created by timotheearnauld on 11/02/2016.
 */
public class EditorController extends DTGController {

    private Stage editorStage;

    @FXML private CheckMenuItem baseButton;
    @FXML private CheckMenuItem shapesButton;
    @FXML private CheckMenuItem tilesButton;


    @FXML private AnchorPane toolbar;
    @FXML private HBox hbox;
    @FXML private BorderPane borderpane;
    @FXML private ScrollPane contentParent;
    @FXML private AnchorPane content;

    private BaseController baseController;
    private ShapesController shapesController;
    private TilesController tilesController;


    public EditorController(){}

    public EditorController(Stage stage) {
	super(stage);
    } 

    @FXML
    public void setBaseOn() throws IOException
    {

        if(baseButton.isSelected())
        {
		
 	    baseController = new BaseController(new Stage());
	    WindowUtility.openStage("/view/main/base.fxml", baseController, StageStyle.UNDECORATED, true);

          

        }

        else
        {
	    WindowUtility.closeStage(baseController, 1);
        }
    }

    @FXML
    public void setShapesOn() throws IOException
    {
        if(shapesButton.isSelected())
        {
	    shapesController = new ShapesController(new Stage());
	    WindowUtility.openStage("/view/main/shapes.fxml", shapesController, StageStyle.UNDECORATED, true);
        }
        else
        {
	   WindowUtility.closeStage(shapesController, 1);            
        }
    }

    @FXML
    public void setTilesOn() throws IOException
    {
        if(tilesButton.isSelected())
        {
		
	    tilesController = new TilesController(new Stage());
	    WindowUtility.openStage("/view/main/tiles.fxml", tilesController, StageStyle.UNDECORATED, true);

        }
        else
        {
	   WindowUtility.closeStage(tilesController, 1);            
        }
    }

    @FXML
    public void disconnectUser()
    {
        try
        {
            returnToMainPane();
        }
        catch (IOException e)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("An error occured. Application is going to close.");
            editorStage.close();
        }
    }

    // Go to the login panel
    public void returnToMainPane() throws IOException {

        SignInController signInController = new SignInController(new Stage());
	WindowUtility.openStage("/view/start/login.fxml", signInController, StageStyle.UNDECORATED, false);

	WindowUtility.closeStage(this, 1);
       
    }
}
