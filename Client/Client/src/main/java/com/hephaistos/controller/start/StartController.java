package com.hephaistos.controller.start;

import com.hephaistos.controller.Controller;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.DirectoryChooser;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

import java.io.IOException;
import java.io.File;

import javafx.application.HostServices;

import com.hephaistos.model.user.User;

/**
 * Created by dim on 03/02/16.
 */
public class StartController extends Controller{

    @FXML private Button signInButton;
   // @FXML private Label createAccountLabel;
   // @FXML private ImageView avatarAccountImage;

    private Stage rootStage;
    private HostServices hostServices;
    private User user;

    public StartController() {
    }

    /*public void setMainApp(Stage rootStage) {
        this.rootStage = rootStage;
    }*/

    public void setHostServices(HostServices hostServices) {
	    this.hostServices = hostServices;
    }

    public void setConnectUser(User user){
	    this.user = user;
    }


    /**
     * @FMLX access from fxml file
     */
	
   /* @FXML
    protected void isConnect(){
	    if(user != null){
		updateConnect();
	    }	    
    }*/
    
   /* @FXML
    public void updateConnect() {
	signInButton.setText("Sign Out");
	createAccountLabel.setVisible(false);
	
	// get avatar image from server todo :  protect	users's images repository
	String urlAvatar = "http://localhost/h-pha-stos-website/H/app/tmp_account/avatar/" + user.getAvatar();

	Image image = new Image(urlAvatar);
	avatarAccountImage.setImage(image);
	avatarAccountImage.setVisible(true);
    }*/ 

    @FXML
    public void showConnectPane() throws IOException {
	
	//User not connected
	if(user ==  null){
		        
        FXMLLoader loader = new FXMLLoader();
	loader.setLocation(StartController.class.getResource("/view/start/signIn.fxml"));
        GridPane signInLayout = loader.load();

        SignInController signInController = loader.getController();
        signInController.setStage(rootStage);
	signInController.setHostServices(hostServices);

        Scene scene = new Scene(signInLayout);
        rootStage.setScene(scene);
        rootStage.show();


	}else{
		//User connected : Sign out
		user = null;
		//Change sign in button
/*		signInButton.setText("Sign In");
		// dispear user avater
		avatarAccountImage.setVisible(false);*/
	}
    }

   // Open dialog window
   @FXML
   public void openProject() {
	
	   DirectoryChooser directoryChooser = new DirectoryChooser();
	   File selectedDirectory = directoryChooser.showDialog(rootStage);
   }

   
}
