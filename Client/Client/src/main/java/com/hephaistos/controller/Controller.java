package com.hephaistos.controller;

<<<<<<< HEAD
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
=======
import com.dtg.graphic.window.DTGWindow;

import com.hephaistos.model.projet.alert.Halert;
import com.hephaistos.model.properties.SceneProperties;
import com.hephaistos.model.properties.StageProperties;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;

>>>>>>> dun

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;

<<<<<<< HEAD
=======
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
>>>>>>> dun
import javafx.stage.Screen;
import javafx.stage.StageStyle;
import javafx.stage.Stage;

<<<<<<< HEAD
import javafx.application.Platform;
import java.io.IOException;




public abstract class Controller {

	private Stage stage;
	private Stage newStage;


	public void setStage(Stage stage) {
		this.stage = stage;
=======
import java.io.IOException;
import java.lang.reflect.Constructor;


public abstract class Controller extends Region{

	protected Stage stage;
	protected Region layout;
	protected StageProperties appProperties = new StageProperties();
	protected StageProperties stageProperties = new StageProperties();

	protected ControllerUtility controllerUtility = new ControllerUtility();


	@FXML
	protected AnchorPane content;

	public Controller() {
		/** Not the best place for this function **/
		getAppProperties();
	}


	public Controller getController() {
		return this;
	}
	public void startStage(){
		stage.show();
>>>>>>> dun
	}

	@FXML
	public void closeApp() {
<<<<<<< HEAD
		
		/** Vérification fermeture application */
		Platform.exit();
		System.exit(0);
	}

	public void closeStage() {
			newStage.close();
	}

	public void openPane(String resourceView, Controller controller) throws IOException {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Controller.class.getResource(resourceView));
		GridPane layout = loader.load();

		Scene scene = new Scene(layout);

		stage.setScene(scene);
		//stage.initStyle(StageStyle.DECORATED);
		stage.show();

		controller = loader.getController();
		controller.setStage(stage);
=======
		alertMessage(this, "confirmation");
	}

	public void closeStage() {
		stage.close();
	}


	public void changeStage(String resourceView, Controller controller, StageProperties props) throws IOException {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Controller.class.getResource(resourceView));


		controller.setStage(new Stage());
		controller.setLayout(loader.load());
		loader.setController(controller);
		//stage.close();


		DTGWindow window = new DTGWindow(controller.getStage(), controller.getLayout(),false);

		window.getStylesheets().add(Controller.class.getResource("/view/dtgWindow/skin/undecorator.css").toString());

		//MDICanvas mdiCanvas = new MDICanvas(MDICanvas.Theme.DARK);



		//content.getChildren().add(mdiCanvas);

		Scene scene = new Scene(window);

		// Transparent scene and stage
		scene.setFill(Color.TRANSPARENT);
		controller.getStage().initStyle(StageStyle.TRANSPARENT);
		controller.getStage().setScene(scene);
		controller.getStage().show();
>>>>>>> dun

		centerRootStage();

	}

<<<<<<< HEAD
	public void openPane(String resourceView, Controller controller, String titlePane) throws IOException {

		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Controller.class.getResource(resourceView));
		GridPane layout = loader.load();

		Scene scene = new Scene(layout);

		newStage = new Stage();
		newStage.setScene(scene);
		newStage.initStyle(StageStyle.DECORATED);
		newStage.setTitle(titlePane);
		newStage.show();

		controller = loader.getController();
		controller.setStage(newStage);
	}


=======
	public void openNewStage(String resourceView, Controller controller,StageProperties props) throws IOException {




		/*FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Controller.class.getResource(resourceView));

		controller.setLayout(loader.load());

		loader.setController(controller);


		createInsideScene(controller, props);*/

	}



	public static void alertMessage(Controller controller, String type){

		Halert alert = new Halert(controller, type);

	}

>>>>>>> dun
	public void centerRootStage() {
		Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
		stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2);
		stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 2);

	}

<<<<<<< HEAD
	public Stage getPrimaryStage() {
		return stage;
	}

	public Controller getController() {
		return this;
	}


=======
	@Deprecated
	public void getAppProperties() {
		/** get application properties form file.properties **/
		Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
		appProperties.setWidth(primScreenBounds.getWidth());
		appProperties.setHeight(primScreenBounds.getHeight());
	}

	public void createInsideScene(Controller controller, StageProperties props) {

		controller.setStage(new Stage());
		DTGWindow window = new DTGWindow(controller.getStage(), controller.getLayout(), false);

		window.getStylesheets().add(Controller.class.getResource("/view/dtgWindow/skin/undecorator.css").toString());
		Scene scene = new Scene(window, props.getWidth(), props.getHeight());
		// Transparent scene and stage
		scene.setFill(Color.TRANSPARENT);
		controller.getStage().initStyle(StageStyle.TRANSPARENT);

		//Scene scene = new Scene(controller.getLayout());


		controller.getStage().setX(props.getLayoutX());
		controller.getStage().setY(props.getLayoutY());

		controller.getStage().setScene(scene);
		controller.getStage().initStyle(props.getStyle());
		controller.getStage().setTitle(props.getTitle());
		controller.getStage().show();
	}

	public void initStageProperties(double width,double height, double layoutX, double layoutY, StageStyle style, String title) {

		stageProperties.setWidth(width);
		stageProperties.setHeight(height);
		stageProperties.setLayoutX(layoutX);
		stageProperties.setLayoutY(layoutY);
		stageProperties.setStyle(style);
		stageProperties.setTitle(title);
	}

	public void initContent() {}
	public StageProperties getStageProperties() {
		return stageProperties;
	}

	public Region getLayout() {
		return layout;
	}

	public void setLayout(Region layout) {
		this.layout = layout;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public Stage getStage() {
		return stage;
	}

	public void setContent(AnchorPane content) {
		this.content = content;
	}

	public AnchorPane getContent() {
		return content;
	}
>>>>>>> dun
}
