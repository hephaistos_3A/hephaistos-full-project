package com.dtg.test;


import com.dtg.graphic.window.DTGScene;
import com.dtg.graphic.window.DTGWindow;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Accordion;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

import java.awt.*;
import java.io.IOException;
import java.net.URI;

public class UndecoratorSceneDemo extends Application {

    Stage primaryStage;
    @FXML
    Accordion accordion;
    @FXML
    HBox clientAreaHbox;
    @FXML
    Slider sliderOpacity;
    @FXML
    Hyperlink hyperlink;

    @Override
    public void start(final Stage stage) throws Exception {
        primaryStage = stage;
        primaryStage.setTitle("Title");


        // The UI (Client Area) to display
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/editor.fxml"));
        fxmlLoader.setController(this);
        Region root = (Region) fxmlLoader.load();

        root.setMinWidth(400);
        root.setMinHeight(400);
        // The Undecorator as a Scene
        final DTGScene dtgScene = new DTGScene(primaryStage, root, false);


        // Enable fade transition
        dtgScene.setFadeInTransition();


        // Optional: Enable this node to drag the stage
        // By default the root argument of Undecorator is set as draggable
//        Node node = root.lookup("#draggableNode");
//        undecoratorScene.setAsStageDraggable(stage, node);

        /*
         * Fade out transition on window closing request
         */
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            public void handle(WindowEvent we) {
                we.consume();   // Do not hide yet
                dtgScene.setFadeOutTransition();
            }
        });



        primaryStage.setScene(dtgScene);
        primaryStage.sizeToScene();
        primaryStage.toFront();

        // Set minimum size based on client area's minimum sizes
        DTGWindow dtgWindow = dtgScene.getDTGWindow();
        primaryStage.setMinWidth(dtgWindow.getMinWidth());
        primaryStage.setMinHeight(dtgWindow.getMinHeight());

        primaryStage.show();
    }

    void initUI() {
        accordion.setExpandedPane(accordion.getPanes().get(1));
        clientAreaHbox.opacityProperty().bind(sliderOpacity.valueProperty());
        hyperlink.setOnAction(new EventHandler<ActionEvent>() {


            public void handle(ActionEvent event) {
                try {
                    Desktop.getDesktop().browse(new URI("https://arnaudnouard.wordpress.com/category/javafx/undecorator/"));
                } catch (Exception ex) {
                }
            }
        });
    }

    /**
     * The button's handler in the ClientArea.fxml Manage the UTILITY mode stage
     *
     * @param event
     */
    @FXML
    private void handleShowUtilityStage(ActionEvent event) throws IOException {
        // Stage Utility usage
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("../../../tmp/demoapp/ClientAreaUtility.fxml"));
        fxmlLoader.setController(this);
        Region root = (Region) fxmlLoader.load();
        Stage utilityStage = new Stage();
        utilityStage.setTitle("Stage Utility type demo");
        DTGScene scene = new DTGScene(utilityStage, StageStyle.UTILITY, root, null, false);
        // Overrides defaults
        scene.addStylesheet("src/tmp/demoapp/demoapp.css");

        utilityStage.setScene(scene);
        utilityStage.initModality(Modality.WINDOW_MODAL);
        utilityStage.initOwner(primaryStage);

        // Set sizes based on client area's sizes
        DTGWindow dtgWindow = scene.getDTGWindow();
        utilityStage.setMinWidth(dtgWindow.getMinWidth());
        utilityStage.setMinHeight(dtgWindow.getMinHeight());
        utilityStage.setWidth(dtgWindow.getPrefWidth());
        utilityStage.setHeight(dtgWindow.getPrefHeight());
        if (dtgWindow.getMaxWidth() > 0) {
            utilityStage.setMaxWidth(dtgWindow.getMaxWidth());
        }
        if (dtgWindow.getMaxHeight() > 0) {
            utilityStage.setMaxHeight(dtgWindow.getMaxHeight());
        }
        utilityStage.sizeToScene();
        utilityStage.show();
    }

    /**
     * Show a non resizable Stage
     *
     * @param event
     */
    @FXML
    private void handleShowNonResizableStage(ActionEvent event) throws Exception {
        UndecoratorSceneDemo undecoratorSceneDemo = new UndecoratorSceneDemo();
        Stage stage = new Stage();
        stage.setTitle("Not resizable stage");
        stage.setResizable(false);
        stage.setWidth(600);
        stage.setMinHeight(400);
        undecoratorSceneDemo.start(stage);
    }

    /**
     * Handles Utility stage buttons
     *
     * @param event
     */
    public void handleUtilityAction(ActionEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
