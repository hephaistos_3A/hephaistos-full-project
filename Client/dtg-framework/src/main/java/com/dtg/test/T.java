package com.dtg.test;


import com.dtg.graphic.utility.WindowUtility;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


/**
 * Created by dim on 14/03/16.
 */
public class T extends Application{



    public void start(Stage stage){

        C c = new C(stage);
        WindowUtility.openStage("/editor.fxml", c, StageStyle.UNDECORATED, true);

    }


    public static void main(String [] args) {
        launch(args);
    }
}
