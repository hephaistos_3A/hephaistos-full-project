package com.dtg.graphic.window;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class DTGScene extends Scene {

    static public final String DEFAULT_STYLESHEET = "skin/DTGWindow.css";
    static public final String DEFAULT_STYLESHEET_UTILITY = "skin/DTGUtilityStage.css";
    static public final String DEFAULT_STAGEDECORATION = "stagedecoration.fxml";
    static public final String DEFAULT_STAGEDECORATION_UTILITY = "stageUtilityDecoration.fxml";
    DTGWindow dtgWindow;

    /**
     * Basic constructor with built-in behavior
     *
     * @param stage The main stage
     * @param root your UI to be displayed in the Stage
     */
    public DTGScene(Stage stage, Region root, boolean inside) {
        this(stage, StageStyle.TRANSPARENT, root, DEFAULT_STAGEDECORATION, inside);
    }

    /**
     * DTGScene constructor
     *
     * @param stage The main stage
     * @param stageStyle could be StageStyle.UTILITY or StageStyle.TRANSPARENT
     * @param root your UI to be displayed in the Stage
     * @param stageDecorationFxml Your own Stage decoration or null to use the
     * built-in one
     */
    public DTGScene(Stage stage, StageStyle stageStyle, Region root, String stageDecorationFxml, boolean inside) {

        super(root);
        
        /*
         * Fxml
         */
        if (stageDecorationFxml == null) {
            if (stageStyle == StageStyle.UTILITY) {
                stageDecorationFxml = DEFAULT_STAGEDECORATION_UTILITY;
            } else {
                stageDecorationFxml = DEFAULT_STAGEDECORATION;
            }
        }
        dtgWindow = new DTGWindow(stage, root, stageDecorationFxml, stageStyle, inside);
        super.setRoot(dtgWindow);
        
        // Customize it by CSS if needed:
        if (stageStyle == StageStyle.UTILITY) {
            dtgWindow.getStylesheets().add(DEFAULT_STYLESHEET_UTILITY);
        } else {
            dtgWindow.getStylesheets().add(DEFAULT_STYLESHEET);
        }

        // Transparent scene and stage
        stage.initStyle(StageStyle.TRANSPARENT);
        super.setFill(Color.TRANSPARENT);
        
        // Default Accelerators
        dtgWindow.installAccelerators(this);
        
    }
    
    public void removeDefaultStylesheet() {
        dtgWindow.getStylesheets().remove(DEFAULT_STYLESHEET);
        dtgWindow.getStylesheets().remove(DEFAULT_STYLESHEET_UTILITY);
    }

    public void addStylesheet(String css) {
        dtgWindow.getStylesheets().add(css);
    }

    public void setAsStageDraggable(Stage stage, Node node) {
        dtgWindow.setAsStageDraggable(stage, node);
    }

    public void setBackgroundStyle(String style) {
        dtgWindow.getBackgroundNode().setStyle(style);
    }
    public void setBackgroundOpacity(double opacity) {
        dtgWindow.getBackgroundNode().setOpacity(opacity);
    }
    public void setBackgroundPaint(Paint paint) {
        dtgWindow.removeDefaultBackgroundStyleClass();
        dtgWindow.getBackgroundNode().setFill(paint);
    }

    public DTGWindow getDTGWindow() {
        return dtgWindow;
    }

    public void setFadeInTransition() {
        dtgWindow.setFadeInTransition();
    }
    public void setFadeOutTransition() {
        dtgWindow.setFadeOutTransition();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
