package com.dtg.graphic.utility;

import com.dtg.controller.DTGController;
import com.dtg.graphic.window.DTGWindow;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

/**
 * Created by dim on 14/03/16.
 */
public class WindowUtility {

    public  static FXMLLoader initView(String resourceView, DTGController controller) {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(controller.getClass().getResource(resourceView));
        loader.setController(controller);

        return loader;
    }

    public static void openStage(String resourceView, DTGController controller, StageStyle style, boolean inside) {

        FXMLLoader loader = initView(resourceView, controller);

        controller.setStage(new Stage());

        try{

            Region root = loader.load();
            DTGWindow dtgWindow = new DTGWindow(controller.getStage(), root, inside);

            dtgWindow.setController(controller);

            dtgWindow.getStylesheets().add(WindowUtility.class.getResource("/skin/DTGWindow.css").toString());

            Scene scene = new Scene(dtgWindow, 400, 400);
            scene.setFill(Color.TRANSPARENT);
            controller.setScene(scene);

            controller.setStageStyle(StageStyle.TRANSPARENT);
            controller.applyScene();

            controller.getStage().show();
        }catch(IOException e){
            System.out.println("Error to load");
            e.printStackTrace();
        }

    }

    public static void closeStage(DTGController controller, int level){

        controller.close(level);
    }
}
