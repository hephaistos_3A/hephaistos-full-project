package com.dtg.controller;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by dim on 13/03/16.
 */
public class DTGController {

    private Stage stage;
    private Scene scene;

    public DTGController(){}

    public DTGController(Stage stage){

        this.stage = stage;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public void setStageStyle(StageStyle style) {

        if(stage != null)
             stage.initStyle(style);
        else{
            /**
             * TODO Exception
             */
            System.out.println("Null Stage");
        }

    }

    /**
     * Apply Scene to Stage
     */
    public void applyScene() {

        if(stage != null && scene != null)
            stage.setScene(scene);
        else{
            /**
             * TODO Exception
             */
            System.out.println("Stage null or Scene null");
        }
    }

    public void close(int level){

        if(stage != null){

            switch(level){
                case 1:
                    stage.close();
                    break;
                case 2:
                    stage.close();
                    Platform.exit();
                    break;
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}


